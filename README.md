# README #

1/3スケールのSHARP MZ-1D22風小物のstlファイルです。 スイッチ類、I/Oポート等は省略しています。

組み合わせたファイルとパーツごとに配置したファイルの二種類があります。 元ファイルはAUTODESK 123D DESIGNです。

***

![](https://bitbucket.org/kyoto-densouan/3dmodel_mz1d22/raw/857163d8f56b8a55225287632da6e6712b9fad5a/ModelView.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_mz1d22/raw/857163d8f56b8a55225287632da6e6712b9fad5a/ModelView_open.png)
![](https://bitbucket.org/kyoto-densouan/3dmodel_mz1d22/raw/857163d8f56b8a55225287632da6e6712b9fad5a/ExampleImage.png)
